import React from 'react';
import './Alert.css';

const Alert = props => {
    return (
        <div className={["alert", props.type, 'alert-dismissible', 'fade show'].join(' ')}
             role="alert"
             style={{
                 transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
                 opacity: props.show ? '1' : '0',
             }}
        >
            {props.children}
            <button type="button"
                    className="btn-close"
                    data-bs-dismiss="alert"
                    aria-label="Close"
                    onClick={props.dismiss}
                    style={{
                        display: props.dismiss ?  'block' : 'none'
                    }}
            > </button>
        </div>
    );
};

export default Alert;