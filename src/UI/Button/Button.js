import React from 'react';
import './Button.css';

const Button = ({onClick, children, type}) => (
    <button type="button"
            className={['btn', type].join(' ')}
            data-dismiss="modal"
            onClick={onClick}>
        {children}
    </button>
);

export default Button;