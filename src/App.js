import './App.css';
import React, {useState} from "react";
import Modal from "./UI/Modal/Modal";
import Button from "./UI/Button/Button";
import Alert from "./UI/Alert/Alert";

const App = () => {
    const [modalShow, setModalShow] = useState(false);
    const [warningAlertShow, setWarningAlertShow] = useState(false);
    const [successAlertShow, setSuccessAlertShow] = useState(false);

    const openModal = () => {
        setModalShow(true);
    };

    const cancelModal = () => {
        setModalShow(false);
    };

    const openWarningAlert = () => {
        setWarningAlertShow(true);
    };

    const openSuccessAlert = () => {
        setSuccessAlertShow(true);
    };

    const someHandler = () => {
        setWarningAlertShow(false);
        };

    return (
        <>
            <Alert
                type="alert-success"
                show={successAlertShow}
            >
                <strong>Success!</strong> This is a success type alert
            </Alert>
            <Alert
                type="alert-warning"
                dismiss={someHandler}
                show={warningAlertShow}
            >
                <strong>Warning!</strong> This is a warning type alert
            </Alert>

            <div className="Buttons">
                <Button onClick={openSuccessAlert} type='btn-success'>Open success alert</Button>
                <Button onClick={openWarningAlert} type='btn-warning'>Open warning alert</Button>
                <Button onClick={openModal} type='btn-primary'>Open modal</Button>
            </div>



            <Modal
                show={modalShow}
                close={cancelModal}
                title="Some kinda modal text"
            >
                <p>Modal body text goes here.</p>
                <Button onClick={cancelModal}
                type='btn-secondary'>
                    Close
                </Button>
            </Modal>
        </>
    )
};
export default App;
